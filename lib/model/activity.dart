import 'package:cloud_firestore/cloud_firestore.dart';

class Activity {
  int actiID;
  String title;
  String body;
  bool active;

  DocumentReference reference;

  Activity.fromMap(Map<String, dynamic> map, {this.reference}) : assert(map['actiID'] != null),
   assert(map['title'] != null),
   assert(map['body'] != null),
   assert(map['active'] != null),
   actiID = map['actiID'],
   title = map['title'],
   body = map['body'],
   active = map['active'];

   Activity.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data, reference: snapshot.reference);
}