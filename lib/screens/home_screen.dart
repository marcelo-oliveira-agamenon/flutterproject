import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:todolist/model/activity.dart';
import 'package:todolist/screens/activity_details.dart';

class HomeScreen extends StatefulWidget{
  @override 
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "App de Atividades"
        ),
      ),
      body: _buildBody(context),
      backgroundColor: Theme.of(context).accentColor,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/addScreen');
        },
        child: Icon(
          Icons.add
        ),
        backgroundColor: Colors.green[500],
      ),
    );
  }

  Widget _buildBody(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection("activities").snapshots(),
      builder: (context, snapshot) {
        if(!snapshot.hasData) return LinearProgressIndicator();

        return _buildList(context, snapshot.data.documents);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      padding: const EdgeInsets.all(10),
      children: snapshot.map((data) => _buildItem(context, data)).toList(),
    );
  }

  Widget _buildItem(BuildContext context, DocumentSnapshot data) {
    final activity = Activity.fromSnapshot(data);

    return Padding(
        key: ValueKey(activity.actiID),
        padding: const EdgeInsets.all(7),
        child: Container(
          height: 50,
          color: Colors.white,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                    left: 25,
                  ),
                  child: Text(
                    '${activity.title}',
                    style: TextStyle(
                      fontSize: 18
                    ),
                  ),
                ),
                FlatButton.icon(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                      builder: (context) => ActivityDetails(
                        activity: activity,
                      )
                    ));
                  },
                  icon: Icon(
                    Icons.info
                  ),
                  label: Text(
                    "Info"
                  ),
                )
              ],
            ),
          ),
        )
      );
  }
}