import 'dart:math';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

enum ActiveProp {
  active, inactive
}

class AddScreenWidget extends StatefulWidget {

  @override
  _AddScreenWidgetState createState() => _AddScreenWidgetState();
}

class _AddScreenWidgetState extends State<AddScreenWidget> {
  final _formKey = GlobalKey<FormState>();
  ActiveProp _act = ActiveProp.active;
  String title;
  String body;
  bool status = true;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text(
            "Adicionar Atividade"
          ),
        ),
        body: _formWidget(context)
      ),
    );
  }

  Map<String, dynamic> _convert(String title, String body, bool status) {
    var rgn = new Random();
    Map<String, dynamic> map = {};
    map['title'] = title;
    map['body'] = body;
    map['active'] = status;
    map['actiID'] = rgn.nextInt(100000);

    return map;
  }

  Widget _formWidget(BuildContext context) {
    Firestore ref = Firestore.instance;

    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 30,
          bottom: 20,
          right: 15,
          left: 15
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            TextFormField(
              onChanged: (value) {
                title = value;
              },
              decoration: const InputDecoration(
                hintText: "Titulo da Atividade"
              ),
              validator: (value) {
                if(value.isEmpty) {
                  return "Digite um titulo";
                }
                return null;
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 25
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    onChanged: (value) {
                      body = value;
                    },
                    decoration: const InputDecoration(
                      hintText: "Descrição da atividade"
                    ),
                    validator: (value) {
                      if(value.isEmpty) {
                        return "Descreva a atividade";
                      }
                      return null;
                    },
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 40,
              ),
              child: Text(
                "Indique se a atividade está ativa ou inativa:"
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 35
              ),
              child: ListTile(
                title: const Text(
                  "Atividade ativa"
                ),
                leading: Radio(
                  value: ActiveProp.active,
                  groupValue: _act,
                  onChanged: (ActiveProp value) {
                    setState(() {
                      _act = value;
                    });
                    status = true;
                  },
                ),
              ),
            ),
             ListTile(
              title: const Text(
                "Atividade inativa"
              ),
              leading: Radio(
                value: ActiveProp.inactive,
                groupValue: _act,
                onChanged: (ActiveProp value) {
                  setState(() {
                    _act = value;
                  });
                  status = false;
                },
              ),
            ),
           Padding(
             padding: const EdgeInsets.all(45),
             child: Row(
               children: <Widget>[
                 FlatButton(
                   child: Text(
                     "Salvar",
                     style: TextStyle(
                       fontSize: 16,
                     ),
                   ),
                   textColor: Colors.white,
                   onPressed: () {
                     ref.collection("activities").add(_convert(title, body, status));
                     Navigator.pushNamed(context, "/");
                   },
                   color: Theme.of(context).accentColor,
                 ),
                 Padding(
                   padding: const EdgeInsets.only(
                     left: 50
                   ),
                   child: FlatButton(
                     child: Text(
                       "Voltar"
                     ),
                     onPressed: () {
                       Navigator.pushNamed(context, "/");
                     },
                     color: Colors.grey[400],
                     textColor: Colors.white,
                   ),
                 )
               ],
             ),
           )
          ],
        ),
      ),
    );
  }
}