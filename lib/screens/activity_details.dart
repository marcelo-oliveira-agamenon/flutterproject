import 'package:flutter/material.dart';
import 'package:todolist/model/activity.dart';

class ActivityDetails extends StatelessWidget {
  final Activity activity;

  ActivityDetails({Key key, @required this.activity}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Atividade"
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                top: 25
              ),
              child: Text(
                '${activity.title}',
                style: TextStyle(
                  fontSize: 25
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 55
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Situação: ${activity.active == true ? "Ativo" : "Inativo"}',
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      backgroundColor: activity.active == true ? Colors.green[600] : Colors.red[600]
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 55
              ),
              child: Text(
                '${activity.body}',
                style: TextStyle(
                  fontSize: 16,
                ),
              ),
            )
          ],
        ),
      )
    );
  }
}